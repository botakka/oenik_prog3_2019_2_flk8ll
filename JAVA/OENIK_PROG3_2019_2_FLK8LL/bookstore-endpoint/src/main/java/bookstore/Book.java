package bookstore;

public class Book {
	private Integer id;
	private String cim;
	private String isbn;
        private String kiado;
	private String kiadasi_ev;
	private Long keszlet;
        private Long szabad;

	public Book() {
	}

	public Book(Integer id, String cim, String isbn, String kiado, String kiadasi_ev, Long keszlet,
			Long szabad) {
		this.id = id;
		this.cim = cim;
		this.isbn = isbn;
		this.kiadasi_ev = kiado;
		this.kiado = kiadasi_ev;
		this.szabad = keszlet;
		this.keszlet = szabad;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCim() {
		return cim;
	}

	public void setCim(String cim) {
		this.cim = cim;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getKiadasi_ev() {
		return kiadasi_ev;
	}

	public void setKiadasi_ev(String kiadasi_ev) {
		this.kiadasi_ev = kiadasi_ev;
	}

	public String getKiado() {
		return kiado;
	}

	public void setKiado(String kiado) {
		this.kiado = kiado;
	}

	public Long getSzabad() {
		return szabad;
	}

	public void setSzabad(Long szabad) {
		this.szabad = szabad;
	}

	public Long getKeszlet() {
		return keszlet;
	}

	public void setKeszlet(Long keszlet) {
		this.keszlet = keszlet;
	}

	@Override
	public String toString() {
		return "Book [id=" + id + ", Cim=" + cim + ", isbn=" + isbn + ", Kiado=" + kiado
				+ ", Kiadasi_ev=" + kiadasi_ev + "Keszlet=" + keszlet + "Szabad=" + szabad + "]";
	}
}

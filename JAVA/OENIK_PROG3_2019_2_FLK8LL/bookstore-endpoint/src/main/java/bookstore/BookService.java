package bookstore;

import org.springframework.stereotype.Service;

import bookstore.BookTemplates;

@Service
public class BookService implements IBookService {

	public BookService() {
	}

	@Override
	public Book find(int id) {
		return BookTemplates.getBook(id);
	}
}

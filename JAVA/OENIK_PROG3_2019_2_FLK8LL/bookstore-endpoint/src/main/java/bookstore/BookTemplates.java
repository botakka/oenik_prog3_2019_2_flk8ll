package bookstore;

public final class BookTemplates {

	private static Book book0 = new Book(0, "Kolduskiraly", "9786150057125", "magankiadas", "2019", 10L, 3L);
	private static Book book1 = new Book(0, "Meg se kinaltak", "9789634793182", "Helikon", "2019", 30L, 10L);
	private static Book book2 = new Book(0, "Testamentumok", "9789636769987", "Jelenkor", "2019", 20L, 10L);
	private static Book book3 = new Book(0, "A szolgalolany meseje", "9789635180578", "Jelenkor", "2019", 10L, 9L);
	private static Book book4 = new Book(0, "Holle anyo birodalma", "9786155808005", "Cerkabella", "2017", 10L, 9L);
	private static Book[] books = { book0, book1, book2, book3, book4 };

	public static Book getBook(int number) {
		int index = number % 5;
		return BookTemplates.books[index];
	}
}

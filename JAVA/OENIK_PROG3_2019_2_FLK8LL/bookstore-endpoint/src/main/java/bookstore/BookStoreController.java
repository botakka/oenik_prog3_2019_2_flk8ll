package bookstore;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.DecimalMin;

@RestController
@RequestMapping("/book")
public class BookStoreController {

	@Autowired
	private IBookService bookService;

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public Book getBook(@PathVariable @DecimalMin("0") Integer id) {
		Book book = bookService.find(id);
		return book;

	}
}
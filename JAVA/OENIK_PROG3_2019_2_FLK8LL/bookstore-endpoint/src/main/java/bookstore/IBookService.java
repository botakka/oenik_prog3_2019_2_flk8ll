package bookstore;

public interface IBookService {
	public Book find(int id);
}

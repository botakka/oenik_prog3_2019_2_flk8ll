﻿// <copyright file="LogicTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyLibary.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using AutoMoq;
    using Moq;
    using MyLibary.Data;
    using MyLibary.Logic;
    using MyLibary.Repository;
    using NUnit.Framework;

    /// <summary>
    /// LogicTest.
    /// </summary>
    [TestFixture]
    public class LogicTest
    {
        private LibaryLogic logic;

        /// <summary>
        /// Init.
        /// </summary>
        [SetUp]
        public void Init()
        {
            var mockRepo = new Mock<ILibaryRepository>();

            List<Konyv> testBookDatas = new List<Konyv>();

            testBookDatas.Add(new Konyv()
            {
                Cim = "Harry Potter:Prisoner of Ascaban",
                id = 10,
            });

            List<Kolcsonze> testHireDatas = new List<Kolcsonze>();
            testHireDatas.Add(new Kolcsonze()
            {
                Datum = DateTime.Today,
                Konyv = 3,
                Hosszabbitva = 4,
            });

            List<Ugyfel> testClientDatas = new List<Ugyfel>();
            testClientDatas.Add(new Ugyfel()
            {
                Lakcim = "Budapest,Flórán tér.8",
                id = 10,
                Foglalkozas = "Orvos",
            });

            mockRepo.Setup(m => m.GetAll()).Returns(testBookDatas.AsQueryable);
            mockRepo.Setup(x => x.GetAllHires()).Returns(testHireDatas.AsQueryable);
            mockRepo.Setup(a => a.GetAllClient()).Returns(testClientDatas.AsQueryable);

            ILibaryRepository mockedRepository = mockRepo.Object;

            this.logic = new LibaryLogic(mockedRepository);
        }

        /// <summary>
        /// Testint Adding Book.
        /// </summary>
        [Test]
        public void TestingInsertingBookEntity()
        {
            Assert.That(
                () => this.logic.AddBook(
                 new Konyv()
                 {
                     Cim = string.Empty,
                     id = 10,
                 }), Throws.TypeOf<ArgumentException>());
        }

        /// <summary>
        /// Testing Hires.
        /// </summary>
        [Test]
        public void TestintInsertingHiresEntity()
        {
            Assert.That(
                () => this.logic.AddHire(
                 new Kolcsonze()
                 {
                     Datum = DateTime.Now,
                     Megjegyzes = "Never read such a good book before!",
                 }), Throws.TypeOf<ArgumentException>());
        }

        /// <summary>
        /// testing client.
        /// </summary>
        [Test]
        public void TestintInsertingClientEntity()
        {
            Assert.That(
                () => this.logic.AddClient(
                 new Ugyfel()
                 {
                     Anyja_neve = "Sophie Vargas",
                     Tel_szam = "06204230487",
                 }), Throws.TypeOf<ArgumentException>());
        }

        /// <summary>
        /// Delete Method.
        /// </summary>
        [Test]
        public void TestingDeleteFromBook()
        {
            var help = this.logic.GetAll().FirstOrDefault(x => x.Cim == "Harry Potter:Prisoner of Ascaban");
            var help2 = this.logic.GetOne(help.id);
            Assert.IsNull(help2);
        }

        /// <summary>
        /// delete from client table.
        /// </summary>
        [Test]
        public void TestingDeleteFromClient()
        {
            var help = this.logic.GetAllClient().FirstOrDefault(x => x.Lakcim == "Budapest,Flórán tér.8");
            long temp = help.id;
            var help2 = this.logic.GetOneClient((int)temp);
            Assert.IsNull(help2);
        }

        /// <summary>
        /// Delete from hire table.
        /// </summary>
        [Test]
        public void TestingDeleteFromHire()
        {
            var help = this.logic.GetAllHires().FirstOrDefault(x => x.Datum.Equals(DateTime.Today));
            var help2 = this.logic.GetOneHireId(help.Konyv1.id);
            Assert.IsNull(help2);
        }

        /// <summary>
        /// Retrieve.
        /// </summary>
        [Test]
        public void TestinReadBook()
        {
            var result = this.logic.GetAll();

            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("Harry Potter:Prisoner of Ascaban", result[0].Cim);
        }

        /// <summary>
        /// Retrieve Client.
        /// </summary>
        [Test]
        public void TestinReadClient()
        {
            var result = this.logic.GetAllClient();

            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual("Budapest,Flórán tér.8", result[0].Lakcim);
        }

        /// <summary>
        /// Retrieve hire.
        /// </summary>
        [Test]
        public void TestinReadHire()
        {
            var result = this.logic.GetAllHires();

            Assert.IsNotNull(result);
            Assert.AreEqual(1, result.Count);
            Assert.AreEqual(DateTime.Today, result[0].Datum);
        }

        /// <summary>
        /// Update Book.
        /// </summary>
        [Test]
        public void UpdateBook()
        {
            var update = new Konyv()
            {
                Cim = "Harry Potter And The Goblet of Fire",
                id = 10,
            };
            var updatedBook = new Konyv()
            {
                Cim = update.Cim,
                id = update.id,
            };

            this.logic.UpdateBookName(update.id, updatedBook.Cim);

            Assert.That(update.id, Is.EqualTo(10));
        }

        /// <summary>
        /// Test UpdateClient.
        /// </summary>
        [Test]
        public void UpdateClient()
        {
            var update = new Ugyfel()
            {
                Lakcim = "Debrecen",
                id = 10,
            };
            var updated = new Ugyfel()
            {
                Lakcim = update.Lakcim,
                id = update.id,
            };
            this.logic.UpdateAdress((int)updated.id, updated.Lakcim);
            Assert.AreEqual(update.id, 10);
        }
    }
}

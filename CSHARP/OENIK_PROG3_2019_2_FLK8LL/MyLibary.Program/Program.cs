﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyLibary.Program
{
    using System.Net;
    using MyLibary.Logic;
    using MyLibary.Repository;
    using Newtonsoft.Json.Linq;

#pragma warning disable SA1600 // Elements should be documented
    internal class Program
#pragma warning restore SA1600 // Elements should be documented
    {
        private static void Main(string[] args)
        {
            ILogic logic = new LibaryLogic(new Repository(new Data.LibaryDatabaseEntities()));
            Menu menu = new Menu();
            menu.MainMenu(logic);
            EndPoint end = new EndPoint();
            end.PageLoad();
        }
    }
}
﻿// <copyright file="EndPoint.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyLibary.Program
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Text;
    using System.Threading.Tasks;
    using System.Web.Script.Serialization;
    using MyLibary.Data;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    /// <summary>
    /// representing endpoint.
    /// </summary>
    internal class EndPoint
    {
        /// <summary>
        /// Try to get json datas from url.
        /// </summary>
        /// <returns>book.</returns>
        public Konyv PageLoad()
        {
            string url = @"http://localhost:8000/book/22";
            WebClient client = new WebClient();
            var json = client.DownloadString(url);
            Konyv book = JsonConvert.DeserializeObject<Konyv>(json);

            return book;
        }
    }
}
